# Evo backend

## Start
Create virtualenv, activate it and install requirements:
```
virtualenv -p /usr/bin/python3 .env
. .env/bin/activate
pip install -r requirements.txt
```