from django.conf.urls import url, include
from django.contrib import admin
from notes import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^notes/', include('notes.urls', namespace='notes')),
    url(r'^$', view=views.notes, name='main'),

]
