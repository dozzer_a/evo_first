from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^create/', view=views.create, name='create'),
    url(r'^notes-list/', view=views.notes_list, name='list'),
]