from django.db import models

from django.dispatch import receiver


# Create your models here.


class Notes(models.Model):

    name = models.CharField(max_length=128, unique=True, null=False, blank=False)
    text = models.TextField(null=False, blank=False)
    unique_words = models.IntegerField(null=True)

    class Meta:
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'

    def __str__(self):
        return self.name


