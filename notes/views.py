from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .forms import NotesForm
from .models import Notes


def notes(request):

    return render(request, 'notes/main.html')


def create(request):

    form = NotesForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        note = form.save()
        text = note.text
        for symbol in [',', '.', '!', '?']:
            text = text.replace(symbol, '')

        note.unique_words = len(set(text.lower().split()))
        note.save()
        return HttpResponseRedirect(request.path)

    return render(request, 'notes/note_create.html', locals())


def notes_list(request):

    sorted_notes = Notes.objects.order_by('-unique_words')
    template = loader.get_template('notes/notes_list.html')
    context = {'sorted_notes': sorted_notes, }

    return HttpResponse(template.render(context))
