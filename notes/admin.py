from django.contrib import admin
from django.contrib.auth.models import Group

from .models import Notes


admin.site.register(Notes)
admin.site.unregister(Group)
